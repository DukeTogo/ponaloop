export class PlayerConstants {
  constructor () {
    const BARS = 4;
    const BEATS = BARS * 4;
    const SIXTEENTHS = BEATS * 4;
  }

}

export default PlayerConstants;