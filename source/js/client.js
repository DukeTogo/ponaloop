import React, { Component } from "react";
import ReactDOM from "react-dom";

import styles from "./components/css/ponaloop.scss"

import GridContainer from "./components/container/GridContainer";

const wrapper = document.getElementById("ponaloop");
wrapper ? ReactDOM.render(<GridContainer />, wrapper) : false;
// (function () {
//   var socket = io();

//   socket.emit('load', {"thing" : "thing"});

//   socket.on('update', (data) => {
//     let pointer = document.getElementById('pointer');

//     pointer.style.top  = `${ data.position.y }px`;
//     pointer.style.left = `${ data.position.x }px`;
//   });

//   document.addEventListener("DOMContentLoaded", function(event) {
//     let playspace = document.getElementById('playspace');

//     playspace.addEventListener('mousemove', function(e) {
//       socket.emit('mousemove', {
//         position: {
//           x: e.x - 10,
//           y: e.y - 10
//         }
//       });
//     });
//   });
// })();
