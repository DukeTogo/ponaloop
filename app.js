require('dotenv').config();

const winston = require('winston');
const express = require('express');
const app     = express();
const http    = require('http').Server(app);
const io      = require('socket.io')(http);



app.use(express.static('public'));

io.on('connection', (socket) => {
  winston.log('info', 'a user connected');

  socket.on('load', (message) => {
    winston.log('info', message);
  });

  socket.on('mousemove', (data) => {
    sendNewCoords(data);
  });
});

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

http.listen(process.env.PORT, () => {
  winston.log('info', `listening on *:${process.env.PORT}`);
});

const sendNewCoords = (coords) => {
  io.emit('update', coords);
};